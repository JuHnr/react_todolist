import { useState } from 'react'
import TaskList from './components/TaskList'

import './App.css'

function App() {

  const tasks = [
    {
      id: 1,
      title: "Faire la vaisselle",
    },
    {
      id: 2,
      title: "Faire les courses",
    },
    {
      id: 3,
      title: "Essence",
    },
  ];

  return (
    <>
      <h1>Ma to-do list</h1>
      <div>
        <TaskList tasks={tasks} />
      </div>
      <div>
        {tasks.map((task) => {
          return <TaskList key={task.id} {...task} />
        })}
      </div>


    </>
  )
}

export default App
